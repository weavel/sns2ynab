import csv
import argparse

input_transactions = []
output_transactions = []
converted = 0
exported = 0

parser = argparse.ArgumentParser()
parser.add_argument("inputcsv", help="The input CSV file from SNS bank")
parser.add_argument("outputcsv", help="path of the output CSV file")

args = parser.parse_args()

try:
    with open(args.inputcsv) as input:
        readCSV = csv.reader(input, delimiter=',')
        for row in readCSV:
            input_transactions.append(row)
except IOError:
    print("The file %s does not exist!" %args.inputcsv)
    quit()
    
for t in input_transactions:
    if t[10] != "0.00":
        #print(t)
        #Transactions of type BEA and COR need their payee name extracted from the commments.
        if t[14] == "BEA" or t[14] == "COR":
            limit = t[17].find(">")
            #print(t[17][1:limit].strip())
            t[3] = t[17][1:limit].strip()
            #print(t[14][limit + 1:254])
            t[17] = t[17][limit + 1:254]
        #strip all unnecesary spaces
        t[17] = t[17].strip()
        #Form the row
        t_out = []
        t_out.append(t[0])
        t_out.append(t[3])
        t_out.append(t[17])
        t_out.append(t[10])
        #print(t_out)
        output_transactions.append(t_out)
        converted += 1
        #print(i)

with open(args.outputcsv, 'w') as output:
    fieldnames = ['date', 'payee', 'memo', 'amount']
    writeCSV = csv.writer(output, delimiter=',')

    writeCSV.writerow(fieldnames)
    for row in output_transactions:
        writeCSV.writerow(row)
