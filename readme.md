# SNS2YNAB Converterscript

As a passionate user of You Need A Budget (YNAB) I like to keep a neat record of all transactions in my banking account. YNAB supports importing transaction data from your bank through QIF, OFX, QFX and CSV. The first three are standards for transaction-information, however Dutch banks seem to be stubborn in supporting these formats for exporting. Most of them support CSV export, however layouts are not uniform accross institutions.

This script is intended for converting CSV-exports from [SNS-bank](https://snsbank.nl) (IBAN: SNSB) to the [YNAB CSV-structure](https://docs.youneedabudget.com/article/921-formatting-csv-file.)
## How to use
This script takes the following arguments:

```
% python3.9 sns2ynab.py -h
usage: sns2ynab.py [-h] inputcsv outputcsv

positional arguments:
  inputcsv    The input CSV file from SNS bank
  outputcsv   path of the output CSV file

optional arguments:
  -h, --help  show this help message and exit
  ```

+ Export transactions from the date-range of your liking to a CSV. There is no official documentation but the procedure is described [in this forum-post (Dutch)](https://forum.snsbank.nl/mijn-sns-sns-mobiel-bankieren-app-69/hoe-maak-ik-een-transactieoverzicht-in-mijn-sns-11704.) 
+ Extract the zip file, and put it somewhere the user that runs this script can open it.
+ Run the sns2ynab.py where `inputcsv` is the path to you inputfile and `outputcsv` is the path to your outputfile. Make sure your user has write permissions for this folder.
+ Use the created CSV for importing in YNAB.

## Possible enhancements
+ Export-filename includes date
+ Export to OFX file structure
+ Maybe build some UI

## Inspiration
+ [ING2OFX by chmistry](https://github.com/chmistry/ing2ofx)
